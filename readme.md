# Pazaak

## Goals:
- The Goal of Pazzak Is to win 3 rounds.
- To Win a round you need to achive a score of 20.
## Main Game
- You Have a side deck that you select from the start of the game.
- Out of the 10 you select, 4 are delt to you at the beginning of the game.
- Those are the only card you can use in that game.
- You can use one side deck card per turn. For more infomation View the Side Deck Section
- On the start of a turn you are delt a card from the main deck.
- Main deck cards have a value of 1-10.
- You have 3 main actions:
	- Flip Card: You flip a flippable card to reverse its value.
	- Stand: Locks in your total, and waits for the other player to bust or hold.
	- End Turn: You end your turn, and are delt a new card on the start of your turn.
## Side Deck
- Special Cards - To Be implemented.
- Plus Cards:
	- Cards that add value to your total when played.
- Minus Cards:
	- Cards that subtract value from your total when played
- Flip Cards:
	- Cards that can flip to positive values to negitive values, and then be played.
