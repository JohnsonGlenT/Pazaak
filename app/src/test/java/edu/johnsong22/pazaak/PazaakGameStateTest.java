package edu.johnsong22.pazaak;

import org.junit.Test;

import static org.junit.Assert.*;

public class PazaakGameStateTest {

    @Test
    public void getPlayer() throws Exception {
        PazaakGameState p = new PazaakGameState();
        p.setPlayer(1);
        assertEquals(p.getPlayer(), 1);
        p.setPlayer(0);
        assertEquals(p.getPlayer(), 0);
    }

    @Test
    public void getNextPlayer() throws Exception {
        PazaakGameState p = new PazaakGameState();
        int lastPlayer = p.getPlayer();
        p.setPlayer(p.getNextPlayer());
        p.setPlayer(p.getNextPlayer());
        assertEquals(lastPlayer, p.getPlayer());

    }

    @Test
    public void onNewTurn() throws Exception {
        PazaakGameState p = new PazaakGameState();
        p.randomDeck();
        p = new PazaakGameState(p, p.firstPlayerDeck, p.firstPlayerDeck);

        int oldVal = p.getPlayer0total();

        p.setPlayer(0);
        p.onNewTurn();
        assertEquals(p.getPlayer0total(), oldVal + p.getPlayer0field()[p.getPlayer0cardsDrawn()-1].getValue());
    }

    @Test
    public void playCard() throws Exception {
        PazaakGameState p = new PazaakGameState();
        p.randomDeck();
        p = new PazaakGameState(p, p.firstPlayerDeck, p.firstPlayerDeck);

        Card card = new Card(true);
        int expected_score = 0;

        int oldVal0 = p.getPlayer0total();
        p.setPlayer(0);
        p.playCard(p.getPlayer(), card);
        assertEquals(p.getPlayer0total(), oldVal0 + card.getValue());

        // tests playing card when not your turn
        p.setPlayer(1);
        p.playCard(0, card);

        int oldVal1 = p.getPlayer1total();

        assertEquals(p.getPlayer1total(), oldVal1);
        assertEquals(p.getPlayer0total(), oldVal0 + card.getValue());

        // test playing used card
        p.getPlayer1side()[1] = null;
        p.setPlayer(1);
        expected_score = p.getPlayer1total();
        p.playCard(1, p.getPlayer1side()[1]);
        assertEquals(p.getPlayer1total(), expected_score);
    }

    @Test
    public void makeNewField() throws Exception{
        PazaakGameState p = new PazaakGameState();
        p.randomDeck();
        p = new PazaakGameState(p, p.firstPlayerDeck, p.firstPlayerDeck);

        Card card = new Card(true);
        p.setPlayer(0);
        p.playCard(p.getPlayer(), card);
        p.setPlayer(1);
        p.playCard(p.getPlayer(), card);
        p.stand(p.getPlayer());
        p.stand(p.getPlayer());
        p.roundWon();
        p.makeNewField();

        for(int i = 0; i < 9; i++){
            assertNull(p.getPlayer0field()[i]);
            assertNull(p.getPlayer1field()[i]);
        }
        assertFalse(p.roundWon());
        assertEquals(p.getPlayer0cardsDrawn(), 0);
        assertEquals(p.getPlayer1cardsDrawn(), 0);
        assertFalse(p.getIsPlayer0Standing());
        assertFalse(p.getIsPlayer1Standing());


    }

    @Test
    public void roundWon_bothHold() throws Exception {
        PazaakGameState p = new PazaakGameState();
        p.randomDeck();
        p = new PazaakGameState(p, p.firstPlayerDeck, p.firstPlayerDeck);

        // First round, both hold, player 1 wins
        p.setPlayer(1);
        Card card = new Card(10, false);
        p.playCard(p.getPlayer(), card);
        p.stand(p.getPlayer());  // player 1 stands
        p.stand(p.getPlayer());  // player 0 stands
        p.roundWon();
        assertEquals(0, p.getPlayer1wins());

    }

    @Test
    public void roundWon_oppBust() throws Exception {
        PazaakGameState p = new PazaakGameState();
        p.randomDeck();
        p = new PazaakGameState(p, p.firstPlayerDeck, p.firstPlayerDeck);

        int expected_rounds_won = 1;
        p.setPlayer(1);
        Card card = new Card(20, false);
        Card cardBust = new Card(22, false);
        p.playCard(p.getPlayer(), card);
        p.stand(p.getPlayer());  // player 1 stands
        p.endTurn(p.getPlayer());
        p.playCard(p.getPlayer(), cardBust);
        p.roundWon();
        assertEquals(p.getPlayer1wins(), expected_rounds_won);

    }

    @Test
    public void roundWon_tie() throws Exception {
        PazaakGameState p = new PazaakGameState();
        p.randomDeck();
        p = new PazaakGameState(p, p.firstPlayerDeck, p.firstPlayerDeck);

        int expected_rounds_won = 0;
        p.setPlayer(1);
        Card card = new Card(20, false);
        p.playCard(p.getPlayer(), card);
        p.stand(p.getPlayer());  // player 1 stands
        p.playCard(p.getPlayer(), card);
        p.stand(p.getPlayer());  // player 0 stands
        p.roundWon();
        assertEquals(p.getPlayer1wins(), expected_rounds_won);

    }

    @Test
    public void stand() throws Exception {
        PazaakGameState p = new PazaakGameState();
        p.setPlayer(0);
        assertTrue(p.stand(p.getPlayer()));
        assertTrue(p.getIsPlayer0Standing());
        p.endTurn(0);

        // Asserts that stand() changed the current player, p0 cannot stand out of turn.
        assertFalse(p.stand(0));

        // Re-checks above, plus that stand() does not make a player standing out of turn.
        p.setPlayer(0);
        assertFalse(p.stand(1));
        assertFalse(p.getIsPlayer1Standing());

    }

    @Test
    public void endTurn() throws Exception {
        PazaakGameState p = new PazaakGameState();
        p.setPlayer(0);
        assertTrue(p.endTurn(0));
        assertEquals(p.getPlayer(), 1);
        assertTrue(p.endTurn(1));
        assertEquals(p.getPlayer(), 0);
        //checks to make sure that only one player can play after another one stands
        p.stand(0);
        assertTrue(p.endTurn(0));
        assertEquals(1, p.getPlayer());
    }
    
    @Test
    public void clearCard() throws Exception {
        PazaakGameState p = new PazaakGameState();
        p.firstPlayerDeck[0] = new Card(1, true, false, false);
        assertNotEquals(p.firstPlayerDeck[0], null);
        p.clearCard(new Card(1, true, false, false));
        assertEquals(p.firstPlayerDeck[0], null);
    }

}
