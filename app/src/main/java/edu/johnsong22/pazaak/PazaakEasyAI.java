package edu.johnsong22.pazaak;

import java.io.Serializable;

import edu.johnsong22.pazaak.GameFramework.GameComputerPlayer;
import edu.johnsong22.pazaak.GameFramework.SelectCardAction;
import edu.johnsong22.pazaak.GameFramework.infoMessage.GameInfo;

/*
    Authors: Glen Johson, Jim Rowe, Glen Stone, James Conn
    Player class for the easy computer player
*/
public class PazaakEasyAI extends GameComputerPlayer implements Serializable {

    //constructor
    public PazaakEasyAI(String name) {
        super(name);
    }

    //computer player receiving info from the local game
    @Override
    protected void receiveInfo(GameInfo info) {
        if (info instanceof PazaakGameState) {
            if (((PazaakGameState) info).getPlayer() != 0) {
                return;
            }

            if (((PazaakGameState) info).secondPlayerDeck != null) {
                int select = (int) (Math.random()*6);
                switch (select) {
                    case 0:
                        game.sendAction(new SelectCardAction(this, new Card(1, true, false, false)));
                        return;
                    case 1:
                        game.sendAction(new SelectCardAction(this, new Card(1, true, false, true)));
                        return;
                    case 2:
                        game.sendAction(new SelectCardAction(this, new Card(3, true, false, false)));
                        return;
                    case 3:
                        game.sendAction(new SelectCardAction(this, new Card(3, true, false, true)));
                        return;
                    case 4:
                        game.sendAction(new SelectCardAction(this, new Card(2, true, false, false)));
                        return;
                    case 5:
                        game.sendAction(new SelectCardAction(this, new Card(2, true, false, true)));
                        return;
                }
                return;
            }

            sleep(1);

            if (((PazaakGameState) info).getPlayer1total() >= 16 &&
                    ((PazaakGameState) info).getPlayer1total() <= 20) {
                game.sendAction(new StandAction(this));
            }

            for (int i = 0; i < 4; i++) {
                if (((PazaakGameState) info).getPlayer1side()[i] != null &&
                        ((PazaakGameState) info).getPlayer1total() + ((PazaakGameState) info).getPlayer1side()[i].value >= 16 &&
                        ((PazaakGameState) info).getPlayer1total() + ((PazaakGameState) info).getPlayer1side()[i].value <= 20 &&
                        !((PazaakGameState) info).isCardPlayed()) {
                    game.sendAction(new PlayCardAction(this, ((PazaakGameState) info).getPlayer1side()[i]));
                    return;
                }
            }

            if (((PazaakGameState) info).getPlayer0total() > ((PazaakGameState) info).getPlayer1total() &&
                    ((PazaakGameState) info).getIsPlayer0Standing()) {
                game.sendAction(new EndTurnAction(this));
            }

            if (((PazaakGameState) info).getPlayer1total() >= 17 && !((PazaakGameState) info).getIsPlayer0Standing()) {
                game.sendAction(new StandAction(this));
            }

            if (((PazaakGameState) info).getPlayer1total() <= 16) {
                game.sendAction(new EndTurnAction(this));
                return;
            }
        }
    }

}
