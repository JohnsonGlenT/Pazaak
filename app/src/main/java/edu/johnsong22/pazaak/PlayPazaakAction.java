package edu.johnsong22.pazaak;

import java.io.Serializable;

import edu.johnsong22.pazaak.GameFramework.GamePlayer;
import edu.johnsong22.pazaak.GameFramework.actionMessage.GameAction;

/*
  Authors: Glen Johson, Jim Rowe, Glen Stone, James Conn
  Action class for starting the game after side deck selection
*/
public class PlayPazaakAction extends GameAction implements Serializable {
  
  //constructor
  public PlayPazaakAction(GamePlayer player){
    super(player);
  }
  
}
