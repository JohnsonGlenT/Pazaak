package edu.johnsong22.pazaak;

/* @authors:
Glen Johnson, Jim Rowe, Grant Stone, James Conn
 */

import java.io.Serializable;
import java.util.ArrayList;

import edu.johnsong22.pazaak.GameFramework.infoMessage.GameState;

public class PazaakGameState extends GameState implements Serializable {

    int count;

    private int player;
    private int playedFirst;

    private boolean justWon;
    private boolean cardPlayed;

    private int player0total;
    private int player1total;

    private int player0wins;
    private int player1wins;

    private boolean isPlayer0standing;
    private boolean isPlayer1standing;

    private int player0cardsDrawn;
    private int player1cardsDrawn;

    private int player0cardsUsed;
    private int player1cardsUsed;

    private Card[] player0field;
    private Card[] player1field;

    private Card[] player0side;
    private Card[] player1side;

    public Card[] firstPlayerDeck;
    public Card[] secondPlayerDeck;

    // Base Constructor
    public PazaakGameState() {
        firstPlayerDeck = new Card[10];
        secondPlayerDeck = new Card[10];
        count = 0;
    }

    // Constructor for choose card state
    public PazaakGameState(Card[] first, Card[] second) {

        firstPlayerDeck = new Card[10];
        secondPlayerDeck = new Card[10];

        for (int i = 0; i < 10; i++) {
            if (first[i] != null) {
                firstPlayerDeck[i] = new Card(first[i]);
            }
        }

        for (int i = 0; i < 10; i++) {
            if (second[i] != null) {
                secondPlayerDeck[i] = new Card(second[i]);
            }
        }

        count = 1;

    }

    // Constructor for game to start
    public PazaakGameState(PazaakGameState pgs, Card[] firstPlayer, Card[] secondPlayer) {
        player0wins = 0;
        player1wins = 0;

        isPlayer0standing = false;
        isPlayer1standing = false;

        justWon = false;
        cardPlayed = false;

        player0cardsDrawn = 0;
        player1cardsDrawn = 0;

        player0cardsUsed = 0;
        player1cardsUsed = 0;

        player = (int) (Math.random()*2);
        playedFirst = player;

        player0total = 0;
        player0field = new Card[9];
        player0side = new Card[4];

        player1total = 0;
        player1field = new Card[9];
        player1side = new Card[4];

        player0side = randomizeCards(firstPlayer);
        player1side = randomizeCards(secondPlayer);

        firstPlayerDeck = null;
        secondPlayerDeck = null;

        onNewTurn();

    }
   
    // Copy Constructor
    public PazaakGameState(PazaakGameState pgs) {
        player0wins = pgs.getPlayer0wins();
        player1wins = pgs.getPlayer1wins();

        justWon = pgs.justWon;
        cardPlayed = pgs.cardPlayed;

        isPlayer0standing = pgs.getIsPlayer0Standing();
        isPlayer1standing = pgs.getIsPlayer1Standing();

        player0cardsDrawn = pgs.getPlayer0cardsDrawn();
        player1cardsDrawn = pgs.getPlayer1cardsDrawn();

        player0cardsUsed = pgs.getPlayer0cardsUsed();
        player1cardsUsed = pgs.getPlayer1cardsUsed();

        this.player = pgs.getNextPlayer();
        playedFirst = player;

        this.player0wins = pgs.getPlayer0wins();
        this.player1wins = pgs.getPlayer1wins();

        this.player0total = pgs.getPlayer0total();
        this.player1total = pgs.getPlayer1total();

        Card[] oldField = pgs.getPlayer0field();
        this.player0field = new Card[9];
        for (int i = 0; i < 9; i++) {
            if (oldField[i] == null) {
                break;
            }
            player0field[i] = new Card(oldField[i]);
        }

        oldField = pgs.getPlayer0side();
        this.player0side = new Card[4];
        for (int i = 0; i < 4; i++) {
            if (oldField[i] == null) {
            } else {
                player0side[i] = new Card(oldField[i]);
            }
        }

        oldField = pgs.getPlayer1field();
        this.player1field = new Card[9];
        for (int i = 0; i < 9; i++) {
            if (oldField[i] == null) {
                break;
            }
            player1field[i] = new Card(oldField[i]);
        }

        oldField = pgs.getPlayer1side();
        this.player1side = new Card[4];
        for (int i = 0; i < 4; i++) {
            if (oldField[i] == null) {
            } else {
                player1side[i] = new Card(oldField[i]);
            }
        }
    }

    // Geters
    public boolean isJustWon() {
        return justWon;
    }

    public int getPlayer() {
        return player;
    }

    public int getNextPlayer() {
        if (player == 1) {
            return 0;
        }
        if (player == 0) {
            return 1;
        }
        return player;
    }

    public int getPlayer0wins() {
        return player0wins;
    }

    public int getPlayer0total() {
        return player0total;
    }

    public int getPlayer0cardsDrawn() {
        return player0cardsDrawn;
    }

    public int getPlayer0cardsUsed() {
        return player0cardsUsed;
    }

    public Card[] getPlayer0field() {
        return player0field;
    }

    public Card[] getPlayer0side() {
        return player0side;
    }

    public int getPlayer1wins() {
        return player1wins;
    }

    public int getPlayer1total() {
        return player1total;
    }

    public Card[] getPlayer1field() {
        return player1field;
    }

    public Card[] getPlayer1side() {
        return player1side;
    }

    public void setPlayer(int player) {
        this.player = player;
    }

    public void addPlayer0cardsDrawn() {
        this.player0cardsDrawn += 1;
    }

    public void addPlayer1cardsDrawn() {
        player1cardsDrawn += 1;
    }

    public void addPlayer0cardsUsed() {
        this.player0cardsUsed += 1;
    }

    public void addPlayer1cardsUsed() {
        this.player1cardsUsed += 1;
    }

    public int getPlayer1cardsDrawn() {
        return player1cardsDrawn;
    }

    public boolean getIsPlayer0Standing() {
        return isPlayer0standing;
    }

    public boolean getIsPlayer1Standing() {
        return isPlayer1standing;
    }

    public int getPlayer1cardsUsed() {
        return player1cardsUsed;
    }

    public boolean isCardPlayed() {
        return cardPlayed;
    }

    // Updating Totals
    public void updateTotals() {
        player0total = 0;
        player1total = 0;
        if (player0cardsDrawn != 0) {
            for (int i = 0; i < player0cardsDrawn; i++) {
                player0total += player0field[i].value;
            }
        }
        if (player1cardsDrawn != 0) {
            for (int i = 0; i < player1cardsDrawn; i++) {
                player1total += player1field[i].value;
            }
        }
    }

    // Plays a card on the start of turn
    public void onNewTurn() {
        if(player == 0 && !isPlayer0standing && player0cardsDrawn != 9) {
            player0field[player0cardsDrawn] = new Card();
            player0cardsDrawn++;
        } else if(player == 1 && !isPlayer1standing && player1cardsDrawn != 9) {
            player1field[player1cardsDrawn] = new Card();
            player1cardsDrawn++;
        }
        updateTotals();
    }

    //  0,   which card to play , any player, player plays a card and tells which card is played,
    //  updating more then just the the return
    public  boolean playCard(int current_player, Card somecard) {
        if (somecard == null) { return false; }
        if (cardPlayed) { return false; }
        if (getPlayer() == current_player) {  // if it is the human players turn,
            if (player0cardsDrawn == 9) {
                return false;
            }
            if (current_player == 0) {
                player0field[player0cardsDrawn] = somecard;
                addPlayer0cardsUsed();
                addPlayer0cardsDrawn();
                cardPlayed = true;
                for (int i = 0; i < 4; i++) {
                    if (player0side[i] == null) { continue; }
                    else if ((somecard.value == player0side[i].value) &&
                            (somecard.fromSideDeck == player0side[i].fromSideDeck) &&
                            (somecard.isNegitive == player0side[i].isNegitive) &&
                            (somecard.isFlippable == player0side[i].isFlippable)) {
                        player0side[i] = null;
                        break;
                    }
                }
            } else {
                if (player1cardsDrawn == 9) {
                    return false;
                }
                player1field[player1cardsDrawn] = somecard;
                addPlayer1cardsUsed();
                addPlayer1cardsDrawn();
                cardPlayed = true;
                for (int i = 0; i < 4; i++) {
                    if (player1side[i] == null) { continue; }
                    else if ((somecard.value == player1side[i].value) &&
                            (somecard.fromSideDeck == player1side[i].fromSideDeck) &&
                            (somecard.isNegitive == player1side[i].isNegitive) &&
                            (somecard.isFlippable == player1side[i].isFlippable)) {
                            player1side[i] = null;
                            break;
                    }
                }
            }
            updateTotals();
            return true;
        } else {
            return false;
        }
    }

    // Puts player into stand state
    public boolean stand(int current_player)
    {
        if (getPlayer() == current_player) {  // if it is the human players turn,
            if (current_player == 0) {
                isPlayer0standing = true;
            } else {
                isPlayer1standing = true;
            }
            return true;
        }
        else
            return false;
    }

    // ends the players turn and sets next player
    public boolean endTurn(int current_player) {
        if (isPlayer0standing && isPlayer1standing) {
            cardPlayed = false;
            return true;
        }
        if (current_player == 1) {
            if(isPlayer0standing) {
                setPlayer(getPlayer());
            } else {
                setPlayer(getNextPlayer());
            }
            cardPlayed = false;
            return true;
        }
        if (current_player == 0) {
            if(isPlayer1standing) {
                setPlayer(getPlayer());
            } else {
                setPlayer(getNextPlayer());
            }
            cardPlayed = false;
            return true;
        }
        return false;
    }

    // calculates who won
    public boolean roundWon() {
        if (player0total > 20){
            player1wins++;
            justWon = true;
            return true;
        }else if(player1total > 20){
            player0wins++;
            justWon = true;
            return true;
        }else if(isPlayer0standing && isPlayer1standing){
            if(player0total > player1total){
                player0wins++;
                justWon = true;
                return true;
            }else if(player1total > player0total) {
                player1wins++;
                justWon = true;
                return true;
            } else {
                justWon = true;
                return true;
            }
        }
        return false;
    }

    // creates a new field
    public void makeNewField() {
        for(int i = 0; i < 9; i++){
            player0field[i] = null;
            player1field[i] = null;
        }
        justWon = false;
        cardPlayed = false;
        player0cardsDrawn = 0;
        player1cardsDrawn = 0;
        isPlayer0standing = false;
        isPlayer1standing = false;
        player = playedFirst;
        playedFirst = getNextPlayer();
    }

    // Forfeits
    public boolean quit() {
        player1wins = 3;
        return true;
    }
    
    // checks for adding  cards
    public boolean preAddCard(Card c) {
        if(firstPlayerDeck == null) { return false; }
        if(c == null) { return false; }
        for(int i = 0; i < 10; i++) {
            if (firstPlayerDeck[i] == null) {
                firstPlayerDeck[i] = c;
                return true;
            }
        }
        return false;
    }

    // check for adding card for cpu
    public boolean preAddCardCPU(Card c) {
        if(secondPlayerDeck == null) { return false; }
        if(c == null) { return false; }
        for(int i = 0; i < 10; i++) {
            if (secondPlayerDeck[i] == null) {
                secondPlayerDeck[i] = c;
                return true;
            }
        }
        return false;
    }

    // removes card from deck
    public boolean clearCard(Card c){
        if (c == null) { return false; }
        for(int i = 0; i < 10; i++){
            if (firstPlayerDeck[i] != null && firstPlayerDeck[i].equals(c)){
                firstPlayerDeck[i] = null;
                return true;
            }
        }
        return false;
    }
    
    // clears deck
    public boolean clearDeck(){
        for(int i = 0; i < 10; i++){
            firstPlayerDeck[i] = null;
        }
        return true;
    }
    
    // create random deck
    public boolean randomDeck(){
        for(int i = 0; i < 10; i++){
            if(firstPlayerDeck[i] == null){
                firstPlayerDeck[i] = new Card(true);
            }
        }
        return true;
    }

    // randomizes card
    public Card[] randomizeCards(Card[] cSel) {
        Card[] sideCards = new Card[4];
        ArrayList<Integer> cardPos = new ArrayList<Integer>();
        while(cardPos.size() < 4){
            int rand = (int) (Math.random() * 10);
            if(!cardPos.contains(rand))
                cardPos.add(rand);
        }

        Card tmp;

        for(int i = 0; !cardPos.isEmpty(); i++){
            tmp = cSel[cardPos.remove(0)];
            if (tmp != null) {
                sideCards[i] = new Card(tmp);
            }
        }
        return sideCards;
    }

}

