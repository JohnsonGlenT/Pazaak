package edu.johnsong22.pazaak;

import java.io.Serializable;

import edu.johnsong22.pazaak.GameFramework.GamePlayer;
import edu.johnsong22.pazaak.GameFramework.actionMessage.GameAction;

/*
    Authors: Grant Stone, James Conn, Glen Johnson, Jim Rowe
    Action class for ending a turn
*/
public class EndTurnAction extends GameAction implements Serializable {
    
    //constructor
    public EndTurnAction(GamePlayer player){
      super(player);  
    }

}
