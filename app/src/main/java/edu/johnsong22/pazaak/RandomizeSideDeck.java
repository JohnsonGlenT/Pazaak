package edu.johnsong22.pazaak;

import java.io.Serializable;

import edu.johnsong22.pazaak.GameFramework.GamePlayer;
import edu.johnsong22.pazaak.GameFramework.actionMessage.GameAction;

/*
  Authors: Glen Johson, Jim Rowe, Glen Stone, James Conn
  Action class for randomizing side deck selection
*/
public class RandomizeSideDeck extends GameAction implements Serializable {
  
  //constructor
  public RandomizeSideDeck(GamePlayer player){
    super(player);
  }
  
}
