package edu.johnsong22.pazaak;

import java.io.Serializable;

import edu.johnsong22.pazaak.GameFramework.GamePlayer;
import edu.johnsong22.pazaak.GameFramework.actionMessage.GameAction;

/*
        Authors: Glen Johson, Jim Rowe, Glen Stone, James Conn
        Action class for starting a turn
*/
public class StartTurnAction extends GameAction implements Serializable {

        //constructor
        public StartTurnAction(GamePlayer player){
            super(player);
        }

}

