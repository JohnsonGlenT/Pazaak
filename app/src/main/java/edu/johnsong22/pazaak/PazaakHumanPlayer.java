package edu.johnsong22.pazaak;

import android.content.Intent;
import android.net.Uri;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.graphics.Color;

import java.io.Serializable;

import edu.johnsong22.pazaak.GameFramework.GameHumanPlayer;
import edu.johnsong22.pazaak.GameFramework.GameMainActivity;
import edu.johnsong22.pazaak.GameFramework.SelectCardAction;
import edu.johnsong22.pazaak.GameFramework.infoMessage.GameInfo;

/*
    Authors: Glen Johson, Jim Rowe, Glen Stone, James Conn
    Player class that controls the GUIs and human interaction
*/
public class PazaakHumanPlayer extends GameHumanPlayer implements Button.OnClickListener, SurfaceView.OnTouchListener, Serializable {

    private Button endTurnButton = null;
    private Button standButton = null;
    private Button forfeitButton = null;
    private Button randomButton = null;
    private Button startButton = null;
    private Button clearButton = null;
    private Button helpButton = null;

    private PlayView playView;
    private DeckView deckView;
    private PazaakGameState pgs;

    private GameMainActivity myActivity;
    private int playerID = getPlayerID();

    //constructor
    public PazaakHumanPlayer(String name) {
        super(name);
    }

    //getter for the GUI
    public View getTopView() {
        return playView;

    }

    //receives info from the local game
    @Override
    public void receiveInfo(GameInfo info) {
        if (info instanceof PazaakGameState) {

            if(((PazaakGameState) info).firstPlayerDeck != null) {
                pgs = new PazaakGameState(((PazaakGameState) info).firstPlayerDeck, ((PazaakGameState) info).secondPlayerDeck);
                deckView.getPGS(pgs);
            } else {
                pgs = new PazaakGameState((PazaakGameState) info);
                playView.getPGS(pgs);
            }

            return;
        }
        flash(Color.RED, 10);
    }

    //control for cards on the card board
    @Override
    public boolean onTouch(View v, MotionEvent me) {

        int[] left = {150, 330, 520, 715, 910, 1105};
        int[] top = {445, 585, 725};

        int[] up = {160, 300, 445, 585, 725};
        int[] xPOS = {1390, 1580};

        int cardHeightDeck = 130;
        int cardWidthDeck = 200;

        if (v == deckView) {
            if ((me.getX() > left[0] && me.getX() < left[0] + cardWidthDeck)
                    && (me.getY() > top[0] && me.getY() < top[0] + cardHeightDeck)) {
                game.sendAction(new SelectCardAction(this, new Card(1, true, false, false)));
            } else if ((me.getX() > left[0] && me.getX() < left[0] + cardWidthDeck)
                    && (me.getY() > top[1] && me.getY() < top[1] + cardHeightDeck)) {
                game.sendAction(new SelectCardAction(this, new Card(1, true, false, true)));
            } else if ((me.getX() > left[0] && me.getX() < left[0] + cardWidthDeck)
                    && (me.getY() > top[2] && me.getY() < top[2] + cardHeightDeck)) {
                game.sendAction(new SelectCardAction(this, new Card(1, true, true, false)));
            } else if ((me.getX() > left[1] && me.getX() < left[1] + cardWidthDeck)
                    && (me.getY() > top[0] && me.getY() < top[0] + cardHeightDeck)) {
                game.sendAction(new SelectCardAction(this, new Card(2, true, false, false)));
            } else if ((me.getX() > left[1] && me.getX() < left[1] + cardWidthDeck)
                    && (me.getY() > top[1] && me.getY() < top[1] + cardHeightDeck)) {
                game.sendAction(new SelectCardAction(this, new Card(2, true, false, true)));
            } else if ((me.getX() > left[1] && me.getX() < left[1] + cardWidthDeck)
                    && (me.getY() > top[2] && me.getY() < top[2] + cardHeightDeck)) {
                game.sendAction(new SelectCardAction(this, new Card(2, true, true, false)));
            } else if ((me.getX() > left[2] && me.getX() < left[2] + cardWidthDeck)
                    && (me.getY() > top[0] && me.getY() < top[0] + cardHeightDeck)) {
                game.sendAction(new SelectCardAction(this, new Card(3, true, false, false)));
            } else if ((me.getX() > left[2] && me.getX() < left[2] + cardWidthDeck)
                    && (me.getY() > top[1] && me.getY() < top[1] + cardHeightDeck)) {
                game.sendAction(new SelectCardAction(this, new Card(3, true, false, true)));
            } else if ((me.getX() > left[2] && me.getX() < left[2] + cardWidthDeck)
                    && (me.getY() > top[2] && me.getY() < top[2] + cardHeightDeck)) {
                game.sendAction(new SelectCardAction(this, new Card(3, true, true, false)));
            } else if ((me.getX() > left[3] && me.getX() < left[3] + cardWidthDeck)
                    && (me.getY() > top[0] && me.getY() < top[0] + cardHeightDeck)) {
                game.sendAction(new SelectCardAction(this, new Card(4, true, false, false)));
            } else if ((me.getX() > left[3] && me.getX() < left[3] + cardWidthDeck)
                    && (me.getY() > top[1] && me.getY() < top[1] + cardHeightDeck)) {
                game.sendAction(new SelectCardAction(this, new Card(4, true, false, true)));
            } else if ((me.getX() > left[3] && me.getX() < left[3] + cardWidthDeck)
                    && (me.getY() > top[2] && me.getY() < top[2] + cardHeightDeck)) {
                game.sendAction(new SelectCardAction(this, new Card(4, true, true, false)));
            } else if ((me.getX() > left[4] && me.getX() < left[4] + cardWidthDeck)
                    && (me.getY() > top[0] && me.getY() < top[0] + cardHeightDeck)) {
                game.sendAction(new SelectCardAction(this, new Card(5, true, false, false)));
            } else if ((me.getX() > left[4] && me.getX() < left[4] + cardWidthDeck)
                    && (me.getY() > top[1] && me.getY() < top[1] + cardHeightDeck)) {
                game.sendAction(new SelectCardAction(this, new Card(5, true, false, true)));
            } else if ((me.getX() > left[4] && me.getX() < left[4] + cardWidthDeck)
                    && (me.getY() > top[2] && me.getY() < top[2] + cardHeightDeck)) {
                game.sendAction(new SelectCardAction(this, new Card(5, true, true, false)));
            } else if ((me.getX() > left[5] && me.getX() < left[5] + cardWidthDeck)
                    && (me.getY() > top[0] && me.getY() < top[0] + cardHeightDeck)) {
                game.sendAction(new SelectCardAction(this, new Card(6, true, false, false)));
            } else if ((me.getX() > left[5] && me.getX() < left[5] + cardWidthDeck)
                    && (me.getY() > top[1] && me.getY() < top[1] + cardHeightDeck)) {
                game.sendAction(new SelectCardAction(this, new Card(6, true, false, true)));
            } else if ((me.getX() > left[5] && me.getX() < left[5] + cardWidthDeck)
                    && (me.getY() > top[2] && me.getY() < top[2] + cardHeightDeck)) {
                game.sendAction(new SelectCardAction(this, new Card(6, true, true, false)));
            } else if ((me.getX() > xPOS[0] && me.getX() < xPOS[0] + cardWidthDeck)
                    && (me.getY() > up[0] && me.getY() < up[0] + cardHeightDeck)) {
                game.sendAction(new ClearCardAction(this, pgs.firstPlayerDeck[0]));
            } else if ((me.getX() > xPOS[0] && me.getX() < xPOS[0] + cardWidthDeck)
                    && (me.getY() > up[1] && me.getY() < up[1] + cardHeightDeck)) {
                game.sendAction(new ClearCardAction(this, pgs.firstPlayerDeck[1]));
            } else if ((me.getX() > xPOS[0] && me.getX() < xPOS[0] + cardWidthDeck)
                    && (me.getY() > up[2] && me.getY() < up[2] + cardHeightDeck)) {
                game.sendAction(new ClearCardAction(this, pgs.firstPlayerDeck[2]));
            } else if ((me.getX() > xPOS[0] && me.getX() < xPOS[0] + cardWidthDeck)
                    && (me.getY() > up[3] && me.getY() < up[3] + cardHeightDeck)) {
                game.sendAction(new ClearCardAction(this, pgs.firstPlayerDeck[3]));
            } else if ((me.getX() > xPOS[0] && me.getX() < xPOS[0] + cardWidthDeck)
                    && (me.getY() > up[4] && me.getY() < up[4] + cardHeightDeck)) {
                game.sendAction(new ClearCardAction(this, pgs.firstPlayerDeck[4]));
            } else if ((me.getX() > xPOS[1] && me.getX() < xPOS[1] + cardWidthDeck)
                    && (me.getY() > up[0] && me.getY() < up[0] + cardHeightDeck)) {
                game.sendAction(new ClearCardAction(this, pgs.firstPlayerDeck[5]));
            } else if ((me.getX() > xPOS[1] && me.getX() < xPOS[1] + cardWidthDeck)
                    && (me.getY() > up[1] && me.getY() < up[1] + cardHeightDeck)) {
                game.sendAction(new ClearCardAction(this, pgs.firstPlayerDeck[6]));
            } else if ((me.getX() > xPOS[1] && me.getX() < xPOS[1] + cardWidthDeck)
                    && (me.getY() > up[2] && me.getY() < up[2] + cardHeightDeck)) {
                game.sendAction(new ClearCardAction(this, pgs.firstPlayerDeck[7]));
            } else if ((me.getX() > xPOS[1] && me.getX() < xPOS[1] + cardWidthDeck)
                    && (me.getY() > up[3] && me.getY() < up[3] + cardHeightDeck)) {
                game.sendAction(new ClearCardAction(this, pgs.firstPlayerDeck[8]));
            } else if ((me.getX() > xPOS[1] && me.getX() < xPOS[1] + cardWidthDeck)
                    && (me.getY() > up[4] && me.getY() < up[4] + cardHeightDeck)) {
                game.sendAction(new ClearCardAction(this, pgs.firstPlayerDeck[9]));
            }
            return false;
        }

        if (v == playView) {
            if ((me.getX() > 120 && me.getX() < 330) && (me.getY() > 720 && me.getY() < 870)) {
                game.sendAction(new PlayCardAction(this, pgs.getPlayer0side()[0]));
            } else if ((me.getX() > 325 && me.getX() < 535) && (me.getY() > 720 && me.getY() < 870)) {
                game.sendAction(new PlayCardAction(this, pgs.getPlayer0side()[1]));
            } else if ((me.getX() > 530 && me.getX() < 740) && (me.getY() > 720 && me.getY() < 870)) {
                game.sendAction(new PlayCardAction(this, pgs.getPlayer0side()[2]));
            } else if ((me.getX() > 740 && me.getX() < 950) && (me.getY() > 720 && me.getY() < 870)) {
                game.sendAction(new PlayCardAction(this, pgs.getPlayer0side()[3]));
            } else if ((me.getX() > 120 && me.getX() < 330) && (me.getY() > 870 && me.getY() < 990)) {
                game.sendAction(new FlipCardAction(this,pgs.getPlayer0side()[0]));
            } else if ((me.getX() > 325 && me.getX() < 535) && (me.getY() > 870 && me.getY() < 990)) {
                game.sendAction(new FlipCardAction(this,pgs.getPlayer0side()[1]));
            } else if ((me.getX() > 530 && me.getX() < 740) && (me.getY() > 870 && me.getY() < 990)) {
                game.sendAction(new FlipCardAction(this,pgs.getPlayer0side()[2]));
            } else if ((me.getX() > 740 && me.getX() < 950) && (me.getY() > 870 && me.getY() < 990)) {
                game.sendAction(new FlipCardAction(this,pgs.getPlayer0side()[3]));
            }
            return false;
        }

        return false;
    }

    //control for buttons that control player actions
    @Override
    public void onClick(View button) {

        if (button.getId() == R.id.help || button.getId() == R.id.helper) {
            String url = "https://github.com/JohnsonGlenT/Pazaak/blob/master/readme.md";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            myActivity.startActivity(i);
            return;
        }

        if (button.getId() == R.id.hold) {
            game.sendAction(new StandAction(this));
            playView.invalidate();
            return;
        }
        if (button.getId() == R.id.endButton) {
            game.sendAction(new EndTurnAction(this));
            playView.invalidate();
            return;
        }
        if (button.getId() == R.id.forfeit) {
            game.sendAction(new QuitAction(this));
            playView.invalidate();
            return;
        }
        if (button.getId() == R.id.random) {
            game.sendAction(new RandomizeSideDeck(this));
            deckView.invalidate();
            return;
        }
        if (button.getId() == R.id.play) {
            game.sendAction(new PlayPazaakAction(this));
            initGame();
            playView.invalidate();
            return;
        }
        if (button.getId() == R.id.clear) {
            game.sendAction(new ClearSideDeck(this));
            deckView.invalidate();
            return;
        }

    }

    //initializes game
    public void initGame() {
        myActivity.setContentView(R.layout.activity_game);

        playView = myActivity.findViewById(R.id.playView);
        standButton = myActivity.findViewById(R.id.hold);
        endTurnButton = myActivity.findViewById(R.id.endButton);
        forfeitButton = myActivity.findViewById(R.id.forfeit);
        helpButton = myActivity.findViewById(R.id.help);

        //Listen for button presses
        playView.setOnTouchListener(this);
        standButton.setOnClickListener(this);
        helpButton.setOnClickListener(this);
        endTurnButton.setOnClickListener(this);
        forfeitButton.setOnClickListener(this);

        playView.postInvalidate();
    }

    //changes GUI between side deck selection and 
    public void setAsGui(GameMainActivity activity) {

        myActivity = activity;

        myActivity.setContentView(R.layout.activity_deck_select);
        deckView = myActivity.findViewById(R.id.deckView);

        randomButton = myActivity.findViewById(R.id.random);
        startButton = myActivity.findViewById(R.id.play);
        clearButton = myActivity.findViewById(R.id.clear);
        helpButton = myActivity.findViewById(R.id.helper);

        randomButton.setOnClickListener(this);
        startButton.setOnClickListener(this);
        clearButton.setOnClickListener(this);
        helpButton.setOnClickListener(this);
        deckView.setOnTouchListener(this);

        pgs = new PazaakGameState();

        setPlayer(pgs.count);
    }

    public int getPlayerID() {
        return playerID;
    }

    public void setPlayer(int player) {
        this.playerID = player;
    }

}
