package edu.johnsong22.pazaak;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;

import java.io.Serializable;

// source from
// https://stackoverflow.com/questions/4861859/implement-sounds-in-an-android-application


/*
    Authors: Glen Johson, Jim Rowe, Glen Stone, James Conn
    Class for playing music while the game runs
*/
public class Music extends Service implements Serializable {

    MediaPlayer player;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    //
    public void onCreate() {
        player = MediaPlayer.create(this, R.raw.star_wars_cantina_band); //select music file
        player.setLooping(true); //set looping
    }
    
    //
    public int onStartCommand(Intent intent, int flags, int startId) {
        player.start();
        return Service.START_NOT_STICKY;
    }

    //
    public void onDestroy() {
        player.stop();
        player.release();
        stopSelf();
        super.onDestroy();
    }

}
