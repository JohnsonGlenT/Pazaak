package edu.johnsong22.pazaak;

import java.io.Serializable;

import edu.johnsong22.pazaak.GameFramework.GameComputerPlayer;
import edu.johnsong22.pazaak.GameFramework.SelectCardAction;
import edu.johnsong22.pazaak.GameFramework.infoMessage.GameInfo;

/*
    Authors: Glen Johson, Jim Rowe, Glen Stone, James Conn
    Player class for the smart computer player
*/
public class PazaakHardAI extends GameComputerPlayer implements Serializable {

    public PazaakHardAI(String name) {
        super(name);
    }

    //receives info from the local game (usually in the form of a PazaakGameState)
    @Override
    protected void receiveInfo(GameInfo info) {
        if (info instanceof PazaakGameState) {
            if (((PazaakGameState) info).getPlayer() != 0) {
                return;
            }

            if (((PazaakGameState) info).secondPlayerDeck != null) {
                int select = (int) (Math.random() * 6);
                switch (select) {
                    case 0:
                        game.sendAction(new SelectCardAction(this, new Card(4, true, true, false)));
                        return;
                    case 1:
                        game.sendAction(new SelectCardAction(this, new Card(4, true, true, true)));
                        return;
                    case 2:
                        game.sendAction(new SelectCardAction(this, new Card(3, true, true, false)));
                        return;
                    case 3:
                        game.sendAction(new SelectCardAction(this, new Card(3, true, true, true)));
                        return;
                    case 4:
                        game.sendAction(new SelectCardAction(this, new Card(2, true, true, false)));
                        return;
                    case 5:
                        game.sendAction(new SelectCardAction(this, new Card(2, true, true, true)));
                        return;
                }
                return;
            }

            sleep(1);

            if (((PazaakGameState) info).getPlayer1total() > 20) {
                for (int i = 0; i < 4; i++) {
                    if (((PazaakGameState) info).getPlayer1side()[i] != null &&
                            ((PazaakGameState) info).getPlayer1side()[i].isFlippable() &&
                            !((PazaakGameState) info).getPlayer1side()[i].isNegitive() &&
                            !((PazaakGameState) info).isCardPlayed()) {
                        ((PazaakGameState) info).getPlayer1side()[i].flip();
                    }
                    if(((PazaakGameState) info).getPlayer1side()[i] != null &&
                            ((PazaakGameState) info).getPlayer1total() + ((PazaakGameState) info).getPlayer1side()[i].value < 20) {
                        game.sendAction(new PlayCardAction(this, ((PazaakGameState) info).getPlayer1side()[i]));
                        return;
                    }
                }
            }

            if(((PazaakGameState) info).getIsPlayer0Standing() &&
                    (((PazaakGameState) info).getPlayer1total() > ((PazaakGameState) info).getPlayer0total())) {
                game.sendAction(new StandAction(this));
                return;
            }

            if (((PazaakGameState) info).getPlayer1total() >= 18 &&
                    ((PazaakGameState) info).getPlayer1total() <= 20) {
                game.sendAction(new StandAction(this));
                return;
            }

            for (int i = 0; i < 4; i++) {
                if(((PazaakGameState) info).getPlayer1side()[i] != null &&
                        ((PazaakGameState) info).getPlayer1total() + ((PazaakGameState) info).getPlayer1side()[i].value >= 18 &&
                        ((PazaakGameState) info).getPlayer1total() + ((PazaakGameState) info).getPlayer1side()[i].value <= 20 &&
                        !((PazaakGameState) info).isCardPlayed()) {
                    game.sendAction(new PlayCardAction(this, ((PazaakGameState) info).getPlayer1side()[i]));
                    return;
                }
            }

            if(((PazaakGameState) info).getPlayer1total() >= 18) {
                game.sendAction(new StandAction(this));
                return;
            }

            if(((PazaakGameState) info).getPlayer1total() <= 17) {
                game.sendAction(new EndTurnAction(this));
                return;
            }

        }
    }
}
