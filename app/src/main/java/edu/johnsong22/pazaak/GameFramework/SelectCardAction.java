package edu.johnsong22.pazaak.GameFramework;

import edu.johnsong22.pazaak.Card;
import edu.johnsong22.pazaak.GameFramework.actionMessage.GameAction;

public class SelectCardAction extends GameAction {

    private Card card;

    public SelectCardAction(GamePlayer player, Card toPlay){
        super(player);
        card = toPlay;
    }

    public Card getCard() {
        return card;
    }

}
