package edu.johnsong22.pazaak;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.SurfaceView;

import java.io.Serializable;

/*
    Authors: Jim Rowe, Grant Stone, Glen Johson, James Conn
    Side Deck Selection GUI that extends the SurfaceView class inherent to Android
*/
public class DeckView extends SurfaceView implements Serializable {

    private PazaakGameState pgs;

    private Paint text;

    private Bitmap background;
    private Bitmap cardNotPlayed;
    private Bitmap mainCards;
    private Bitmap addCards;
    private Bitmap minusCards;
    private Bitmap flipCardsPos;
    private Bitmap flipCardsNeg;
    private Bitmap specialCards;

    int height;
    int width;

    int cardHeight;
    int cardWidth;
    
    //constructor
    public DeckView(Context context, AttributeSet attrs) {
        super(context, attrs);

        height = 1000;
        width = 1925;

        cardHeight = 130;
        cardWidth = 200;

        text = new Paint();
        text.setStrokeWidth(5.0f);
        text.setTextSize(28);
        text.setColor(Color.WHITE);

        background = BitmapFactory.decodeResource(getResources(), R.drawable.selection_background);
        background = Bitmap.createScaledBitmap(background, width, height, true);

        cardNotPlayed = BitmapFactory.decodeResource(getResources(), R.drawable.lbl_card_back);
        cardNotPlayed = Bitmap.createScaledBitmap(cardNotPlayed, cardWidth, cardHeight, true);

        mainCards = BitmapFactory.decodeResource(getResources(), R.drawable.lbl_card_stand);
        mainCards = Bitmap.createScaledBitmap(mainCards, cardWidth, cardHeight, true);

        addCards = BitmapFactory.decodeResource(getResources(), R.drawable.lbl_card_plus);
        addCards = Bitmap.createScaledBitmap(addCards, cardWidth, cardHeight, true);

        minusCards = BitmapFactory.decodeResource(getResources(), R.drawable.lbl_card_minus);
        minusCards = Bitmap.createScaledBitmap(minusCards, cardWidth, cardHeight, true);

        flipCardsPos = BitmapFactory.decodeResource(getResources(), R.drawable.lbl_card_flip_plus);
        flipCardsPos = Bitmap.createScaledBitmap(flipCardsPos, cardWidth, cardHeight, true);

        flipCardsNeg = BitmapFactory.decodeResource(getResources(), R.drawable.lbl_card_flip_minus);
        flipCardsNeg = Bitmap.createScaledBitmap(flipCardsNeg, cardWidth, cardHeight, true);

        specialCards = BitmapFactory.decodeResource(getResources(), R.drawable.lbl_card_special);
        specialCards = Bitmap.createScaledBitmap(specialCards, cardWidth, cardHeight, true);

        setWillNotDraw(false);
    }
    
    //method for drawing the GUI
    public void onDraw(Canvas canvas) {
        canvas.drawBitmap(background, 0 , 0, null);
        drawCards(canvas);
        drawSideDeck(canvas);
        invalidate();
    }
    
    //method for displaying the cards during side deck selection
    public void drawCards(Canvas canvas) {
        //SPECIAL ROW
        canvas.drawBitmap(specialCards, 150, 300, null);
        canvas.drawBitmap(specialCards, 330, 300, null);
        canvas.drawBitmap(specialCards, 520, 300, null);
        canvas.drawBitmap(specialCards, 715, 300, null);
        canvas.drawBitmap(specialCards, 910, 300, null);

        //PLUS ROW
        canvas.drawBitmap(addCards, 150, 445, null);
        canvas.drawText("1", 150 + (float) (cardWidth / 2) - 4.0f,
                445 + (float) (cardHeight / 2) - 1.5f, text);
        canvas.drawBitmap(addCards, 330, 445, null);
        canvas.drawText("2", 330 + (float) (cardWidth / 2) - 4.0f,
                445 + (float) (cardHeight / 2) - 1.5f, text);
        canvas.drawBitmap(addCards, 520, 445, null);
        canvas.drawText("3", 520 + (float) (cardWidth / 2) - 4.0f,
                445 + (float) (cardHeight / 2) - 1.5f, text);
        canvas.drawBitmap(addCards, 715, 445, null);
        canvas.drawText("4", 715 + (float) (cardWidth / 2) - 4.0f,
                445 + (float) (cardHeight / 2) - 1.5f, text);
        canvas.drawBitmap(addCards, 910, 445, null);
        canvas.drawText("5", 910 + (float) (cardWidth / 2) - 4.0f,
                445 + (float) (cardHeight / 2) - 1.5f, text);
        canvas.drawBitmap(addCards, 1105, 445, null);
        canvas.drawText("6", 1105 + (float) (cardWidth / 2) - 4.0f,
                445 + (float) (cardHeight / 2) - 1.5f, text);

        //MINUS ROW
        canvas.drawBitmap(minusCards, 150, 585, null);
        canvas.drawText("1", 150 + (float) (cardWidth / 2) - 4.0f,
                585 + (float) (cardHeight / 2) - 1.5f, text);
        canvas.drawBitmap(minusCards, 330, 585, null);
        canvas.drawText("2", 330 + (float) (cardWidth / 2) - 4.0f,
                585 + (float) (cardHeight / 2) - 1.5f, text);
        canvas.drawBitmap(minusCards, 520, 585, null);
        canvas.drawText("3", 520 + (float) (cardWidth / 2) - 4.0f,
                585 + (float) (cardHeight / 2) - 1.5f, text);
        canvas.drawBitmap(minusCards, 715, 585, null);
        canvas.drawText("4", 715 + (float) (cardWidth / 2) - 4.0f,
                585 + (float) (cardHeight / 2) - 1.5f, text);
        canvas.drawBitmap(minusCards, 910, 585, null);
        canvas.drawText("5", 910 + (float) (cardWidth / 2) - 4.0f,
                585 + (float) (cardHeight / 2) - 1.5f, text);
        canvas.drawBitmap(minusCards, 1105, 585, null);
        canvas.drawText("6", 1105 + (float) (cardWidth / 2) - 4.0f,
                585 + (float) (cardHeight / 2) - 1.5f, text);

        //FLIP ROW
        canvas.drawBitmap(flipCardsPos, 150, 725, null);
        canvas.drawText("1", 150 + (float) (cardWidth / 2) - 4.0f,
                725 + (float) (cardHeight / 2) - 1.5f, text);
        canvas.drawBitmap(flipCardsPos, 330, 725, null);
        canvas.drawText("2", 330 + (float) (cardWidth / 2) - 4.0f,
                725 + (float) (cardHeight / 2) - 1.5f, text);
        canvas.drawBitmap(flipCardsPos, 520, 725, null);
        canvas.drawText("3", 520 + (float) (cardWidth / 2) - 4.0f,
                725 + (float) (cardHeight / 2) - 1.5f, text);
        canvas.drawBitmap(flipCardsPos, 715, 725, null);
        canvas.drawText("4", 715 + (float) (cardWidth / 2) - 4.0f,
                725 + (float) (cardHeight / 2) - 1.5f, text);
        canvas.drawBitmap(flipCardsPos, 910, 725, null);
        canvas.drawText("5", 910 + (float) (cardWidth / 2) - 4.0f,
                725 + (float) (cardHeight / 2) - 1.5f, text);
        canvas.drawBitmap(flipCardsPos, 1105, 725, null);
        canvas.drawText("6", 1105 + (float) (cardWidth / 2) - 4.0f,
                725 + (float) (cardHeight / 2) - 1.5f, text);

    }
    
    //method for displaying the selected side deck cards
    public void drawSideDeck(Canvas canvas) {

        if(pgs == null) {
            canvas.drawBitmap(cardNotPlayed, 1390, 160, null);
            canvas.drawBitmap(cardNotPlayed, 1390, 300, null);
            canvas.drawBitmap(cardNotPlayed, 1390, 445, null);
            canvas.drawBitmap(cardNotPlayed, 1390, 585, null);
            canvas.drawBitmap(cardNotPlayed, 1390, 725, null);

            //EVEN CARDS
            canvas.drawBitmap(cardNotPlayed, 1580, 160, null);
            canvas.drawBitmap(cardNotPlayed, 1580, 300, null);
            canvas.drawBitmap(cardNotPlayed, 1580, 445, null);
            canvas.drawBitmap(cardNotPlayed, 1580, 585, null);
            canvas.drawBitmap(cardNotPlayed, 1580, 725, null);
            return;
        }

        if (pgs.firstPlayerDeck[0] == null) {
            canvas.drawBitmap(cardNotPlayed, 1390, 160, null);
        } else if (pgs.firstPlayerDeck[0].isFlippable) {
            canvas.drawBitmap(flipCardsPos, 1390, 160, null);
            canvas.drawText(String.valueOf(pgs.firstPlayerDeck[0].getValue()),
                    1390 + (float) (cardWidth / 2) - 4.0f, 160 + (float) (cardHeight / 2) - 1.5f, text);
        } else if (pgs.firstPlayerDeck[0].isNegitive) {
            canvas.drawBitmap(minusCards, 1390, 160, null);
            canvas.drawText(String.valueOf(pgs.firstPlayerDeck[0].getValue()),
                    1390 + (float) (cardWidth / 2) - 4.0f, 160 + (float) (cardHeight / 2) - 1.5f, text);
        } else {
            canvas.drawBitmap(addCards, 1390, 160, null);
            canvas.drawText(String.valueOf(pgs.firstPlayerDeck[0].getValue()),
                    1390 + (float) (cardWidth / 2) - 4.0f, 160 + (float) (cardHeight / 2) - 1.5f, text);
        }

        if (pgs.firstPlayerDeck[1] == null) {
            canvas.drawBitmap(cardNotPlayed, 1390, 300, null);
        } else if (pgs.firstPlayerDeck[1].isFlippable) {
            canvas.drawBitmap(flipCardsPos, 1390, 300, null);
            canvas.drawText(String.valueOf(pgs.firstPlayerDeck[1].getValue()),
                    1390 + (float) (cardWidth / 2) - 4.0f, 300 + (float) (cardHeight / 2) - 1.5f, text);
        } else if (pgs.firstPlayerDeck[1].isNegitive) {
            canvas.drawBitmap(minusCards, 1390, 300, null);
            canvas.drawText(String.valueOf(pgs.firstPlayerDeck[1].getValue()),
                    1390 + (float) (cardWidth / 2) - 4.0f, 300 + (float) (cardHeight / 2) - 1.5f, text);
        } else {
            canvas.drawBitmap(addCards, 1390, 300, null);
            canvas.drawText(String.valueOf(pgs.firstPlayerDeck[1].getValue()),
                    1390 + (float) (cardWidth / 2) - 4.0f, 300 + (float) (cardHeight / 2) - 1.5f, text);
        }

        if (pgs.firstPlayerDeck[2] == null) {
            canvas.drawBitmap(cardNotPlayed, 1390, 445, null);
        } else if (pgs.firstPlayerDeck[2].isFlippable) {
            canvas.drawBitmap(flipCardsPos, 1390, 445, null);
            canvas.drawText(String.valueOf(pgs.firstPlayerDeck[2].getValue()),
                    1390 + (float) (cardWidth / 2) - 4.0f, 445 + (float) (cardHeight / 2) - 1.5f, text);
        } else if (pgs.firstPlayerDeck[2].isNegitive) {
            canvas.drawBitmap(minusCards, 1390, 445, null);
            canvas.drawText(String.valueOf(pgs.firstPlayerDeck[2].getValue()),
                    1390 + (float) (cardWidth / 2) - 4.0f, 445 + (float) (cardHeight / 2) - 1.5f, text);
        } else {
            canvas.drawBitmap(addCards, 1390, 445, null);
            canvas.drawText(String.valueOf(pgs.firstPlayerDeck[2].getValue()),
                    1390 + (float) (cardWidth / 2) - 4.0f, 445 + (float) (cardHeight / 2) - 1.5f, text);
        }

        if (pgs.firstPlayerDeck[3] == null) {
            canvas.drawBitmap(cardNotPlayed, 1390, 585, null);
        } else if (pgs.firstPlayerDeck[3].isFlippable) {
            canvas.drawBitmap(flipCardsPos, 1390, 585, null);
            canvas.drawText(String.valueOf(pgs.firstPlayerDeck[3].getValue()),
                    1390 + (float) (cardWidth / 2) - 4.0f, 585 + (float) (cardHeight / 2) - 1.5f, text);
        } else if (pgs.firstPlayerDeck[3].isNegitive) {
            canvas.drawBitmap(minusCards, 1390, 585, null);
            canvas.drawText(String.valueOf(pgs.firstPlayerDeck[3].getValue()),
                    1390 + (float) (cardWidth / 2) - 4.0f, 585 + (float) (cardHeight / 2) - 1.5f, text);
        } else {
            canvas.drawBitmap(addCards, 1390, 585, null);
            canvas.drawText(String.valueOf(pgs.firstPlayerDeck[3].getValue()),
                    1390 + (float) (cardWidth / 2) - 4.0f, 585 + (float) (cardHeight / 2) - 1.5f, text);

        }

        if (pgs.firstPlayerDeck[4] == null) {
            canvas.drawBitmap(cardNotPlayed, 1390, 725, null);
        } else if (pgs.firstPlayerDeck[4].isFlippable) {
            canvas.drawBitmap(flipCardsPos, 1390, 725, null);
            canvas.drawText(String.valueOf(pgs.firstPlayerDeck[4].getValue()),
                    1390 + (float) (cardWidth / 2) - 4.0f, 725 + (float) (cardHeight / 2) - 1.5f, text);
        } else if (pgs.firstPlayerDeck[4].isNegitive) {
            canvas.drawBitmap(minusCards, 1390, 725, null);
            canvas.drawText(String.valueOf(pgs.firstPlayerDeck[4].getValue()),
                    1390 + (float) (cardWidth / 2) - 4.0f, 725 + (float) (cardHeight / 2) - 1.5f, text);
        } else {
            canvas.drawBitmap(addCards, 1390, 725, null);
            canvas.drawText(String.valueOf(pgs.firstPlayerDeck[4].getValue()),
                    1390 + (float) (cardWidth / 2) - 4.0f, 725 + (float) (cardHeight / 2) - 1.5f, text);
        }

        if (pgs.firstPlayerDeck[5] == null) {
            canvas.drawBitmap(cardNotPlayed, 1580, 160, null);
        } else if (pgs.firstPlayerDeck[5].isFlippable) {
            canvas.drawBitmap(flipCardsPos, 1580, 160, null);
            canvas.drawText(String.valueOf(pgs.firstPlayerDeck[5].getValue()),
                    1580 + (float) (cardWidth / 2) - 4.0f, 160 + (float) (cardHeight / 2) - 1.5f, text);
        } else if (pgs.firstPlayerDeck[5].isNegitive) {
            canvas.drawBitmap(minusCards, 1580, 160, null);
            canvas.drawText(String.valueOf(pgs.firstPlayerDeck[5].getValue()),
                    1580 + (float) (cardWidth / 2) - 4.0f, 160 + (float) (cardHeight / 2) - 1.5f, text);
        } else {
            canvas.drawBitmap(addCards, 1580, 160, null);
            canvas.drawText(String.valueOf(pgs.firstPlayerDeck[5].getValue()),
                    1580 + (float) (cardWidth / 2) - 4.0f, 160 + (float) (cardHeight / 2) - 1.5f, text);
        }

        if (pgs.firstPlayerDeck[6] == null) {
            canvas.drawBitmap(cardNotPlayed, 1580, 300, null);
        } else if (pgs.firstPlayerDeck[6].isFlippable) {
            canvas.drawBitmap(flipCardsPos, 1580, 300, null);
            canvas.drawText(String.valueOf(pgs.firstPlayerDeck[6].getValue()),
                    1580 + (float) (cardWidth / 2) - 4.0f, 300 + (float) (cardHeight / 2) - 1.5f, text);
        } else if (pgs.firstPlayerDeck[6].isNegitive) {
            canvas.drawBitmap(minusCards, 1580, 300, null);
            canvas.drawText(String.valueOf(pgs.firstPlayerDeck[6].getValue()),
                    1580 + (float) (cardWidth / 2) - 4.0f, 300 + (float) (cardHeight / 2) - 1.5f, text);
        } else {
            canvas.drawBitmap(addCards, 1580, 300, null);
            canvas.drawText(String.valueOf(pgs.firstPlayerDeck[6].getValue()),
                    1580 + (float) (cardWidth / 2) - 4.0f, 300 + (float) (cardHeight / 2) - 1.5f, text);
        }

        if (pgs.firstPlayerDeck[7] == null) {
            canvas.drawBitmap(cardNotPlayed, 1580, 445, null);
        } else if (pgs.firstPlayerDeck[7].isFlippable) {
            canvas.drawBitmap(flipCardsPos, 1580, 445, null);
            canvas.drawText(String.valueOf(pgs.firstPlayerDeck[7].getValue()),
                    1580 + (float) (cardWidth / 2) - 4.0f, 445 + (float) (cardHeight / 2) - 1.5f, text);
        } else if (pgs.firstPlayerDeck[7].isNegitive) {
            canvas.drawBitmap(minusCards, 1580, 445, null);
            canvas.drawText(String.valueOf(pgs.firstPlayerDeck[7].getValue()),
                    1580 + (float) (cardWidth / 2) - 4.0f, 445 + (float) (cardHeight / 2) - 1.5f, text);
        } else {
            canvas.drawBitmap(addCards, 1580, 445, null);
            canvas.drawText(String.valueOf(pgs.firstPlayerDeck[7].getValue()),
                    1580 + (float) (cardWidth / 2) - 4.0f, 445 + (float) (cardHeight / 2) - 1.5f, text);
        }

        if (pgs.firstPlayerDeck[8] == null) {
            canvas.drawBitmap(cardNotPlayed, 1580, 585, null);
        } else if (pgs.firstPlayerDeck[8].isFlippable) {
            canvas.drawBitmap(flipCardsPos, 1580, 585, null);
            canvas.drawText(String.valueOf(pgs.firstPlayerDeck[8].getValue()),
                    1580 + (float) (cardWidth / 2) - 4.0f, 585 + (float) (cardHeight / 2) - 1.5f, text);
        } else if (pgs.firstPlayerDeck[8].isNegitive) {
            canvas.drawBitmap(minusCards, 1580, 585, null);
            canvas.drawText(String.valueOf(pgs.firstPlayerDeck[8].getValue()),
                    1580 + (float) (cardWidth / 2) - 4.0f, 585 + (float) (cardHeight / 2) - 1.5f, text);
        } else {
            canvas.drawBitmap(addCards, 1580, 585, null);
            canvas.drawText(String.valueOf(pgs.firstPlayerDeck[8].getValue()),
                    1580 + (float) (cardWidth / 2) - 4.0f, 585 + (float) (cardHeight / 2) - 1.5f, text);
        }

        if (pgs.firstPlayerDeck[9] == null) {
            canvas.drawBitmap(cardNotPlayed, 1580, 725, null);
        } else if (pgs.firstPlayerDeck[9].isFlippable) {
            canvas.drawBitmap(flipCardsPos, 1580, 725, null);
            canvas.drawText(String.valueOf(pgs.firstPlayerDeck[9].getValue()),
                    1580 + (float) (cardWidth / 2) - 4.0f, 725 + (float) (cardHeight / 2) - 1.5f, text);
        } else if (pgs.firstPlayerDeck[9].isNegitive) {
            canvas.drawBitmap(minusCards, 1580, 725, null);
            canvas.drawText(String.valueOf(pgs.firstPlayerDeck[9].getValue()),
                    1580 + (float) (cardWidth / 2) - 4.0f, 725 + (float) (cardHeight / 2) - 1.5f, text);
        } else {
            canvas.drawBitmap(addCards, 1580, 725, null);
            canvas.drawText(String.valueOf(pgs.firstPlayerDeck[9].getValue()),
                    1580 + (float) (cardWidth / 2) - 4.0f, 725 + (float) (cardHeight / 2) - 1.5f, text);
        }

    }

    //getter for the game state
    public void getPGS(PazaakGameState PGS) {
        pgs = PGS;
    }

    @Override
    public boolean performClick() {
        super.performClick();
        return true;
    }
}
