package edu.johnsong22.pazaak;

import java.io.Serializable;

/*
@authors Glen Johnson, James Conn, Jim Rowe, Grant Stone
*/
import edu.johnsong22.pazaak.GameFramework.GamePlayer;
import edu.johnsong22.pazaak.GameFramework.SelectCardAction;
import edu.johnsong22.pazaak.GameFramework.actionMessage.GameAction;
import edu.johnsong22.pazaak.GameFramework.LocalGame;

public class PazaakLocalGame extends LocalGame implements Serializable {

    PazaakGameState pGS;

    // Constructor
    public PazaakLocalGame() {
        pGS = new PazaakGameState();

    }
   
    // Checks if player can make move
    protected boolean canMove(int playerID){
        return playerID == pGS.getPlayer();
    }

    @Override
    public boolean makeMove(GameAction action) {
        // looks at actions sent to game state
        if (action instanceof PlayCardAction) {
            pGS.playCard(pGS.getPlayer(), ((PlayCardAction) action).getCard());
            return true;
        } else if (action instanceof EndTurnAction) {
            pGS.endTurn(pGS.getPlayer());
            pGS.roundWon();
            if (pGS.isJustWon()) {
                pGS.makeNewField();
            }
            pGS.onNewTurn();
            return true;
        } else if (action instanceof StandAction) {
            pGS.stand(pGS.getPlayer());
            pGS.endTurn(pGS.getPlayer());
            pGS.roundWon();
            if (pGS.isJustWon()) {
                pGS.makeNewField();
            }
            pGS.onNewTurn();
            return true;
        } else if (action instanceof StartTurnAction) {
            pGS.onNewTurn();
            return true;
        } else if (action instanceof RoundWonAction) {
            pGS.roundWon();
            if (pGS.isJustWon()) {
                pGS.makeNewField();
            }
            return true;
        } else if(action instanceof NewFieldAction) {
            pGS.makeNewField();
            return true;
        } else if(action instanceof QuitAction){
            pGS.quit();
            return true;
        } else if (action instanceof SelectCardAction) {
            // checks if we can add card before adding card
            pGS.preAddCard(((SelectCardAction) action).getCard());
            int select = (int) (Math.random()*6);
            switch (select) {
                case 0:
                    pGS.preAddCardCPU(new Card(1, true, false, false));
                    return true;
                case 1:
                    pGS.preAddCardCPU(new Card(new Card(1, true, false, true)));
                    return true;
                case 2:
                    pGS.preAddCardCPU(new Card(new Card(3, true, false, false)));
                    return true;
                case 3:
                    pGS.preAddCardCPU(new Card(new Card(3, true, false, true)));
                    return true;
                case 4:
                    pGS.preAddCardCPU(new Card(new Card(2, true, false, false)));
                    return true;
                case 5:
                    pGS.preAddCardCPU(new Card(new Card(2, true, false, true)));
                    return true;
            }

            return true;

        } else if (action instanceof ClearCardAction) {
            pGS.clearCard(((ClearCardAction) action).getCard());
            return true;
        } else if (action instanceof ClearSideDeck) {
            pGS.clearDeck();
            return true;
        } else if (action instanceof RandomizeSideDeck) {
            pGS.randomDeck();
            return true;
        } else if (action instanceof PlayPazaakAction) {
            // starts game
            for(int i = 0; i < 10; i++) {
                if (pGS.firstPlayerDeck[i] == null) {
                    pGS.randomDeck();
                }
                if (pGS.secondPlayerDeck[i] == null) {
                    pGS.secondPlayerDeck[i] = new Card(true);
                }
            }
            pGS = new PazaakGameState(pGS, pGS.firstPlayerDeck, pGS.secondPlayerDeck);
            return true;
        } else if (action instanceof FlipCardAction) {
            for (int i = 0; i < 4; i++) {
                if (pGS.getPlayer0side()[i] == null) { continue; }
                else if (pGS.getPlayer0side()[i].equals(((FlipCardAction) action).getCard()) &&
                        pGS.getPlayer0side()[i].isFlippable()) {
                    pGS.getPlayer0side()[i].flip();
                    return true;
                }
            }
            return false;
        } else {
            return false;
        }
    }

    // sends the game updated game state to the players
    @Override
    public void sendUpdatedStateTo(GamePlayer p){
        if (pGS.firstPlayerDeck != null) {
            PazaakGameState pgsCpy = new PazaakGameState(pGS.firstPlayerDeck, pGS.secondPlayerDeck);
            p.sendInfo(pgsCpy);
        } else {
            PazaakGameState pgsCpy = new PazaakGameState(pGS);
            p.sendInfo(pgsCpy);
        }
    }

    // checks if game is over
    @Override
    protected String checkIfGameOver(){
        if(pGS.getPlayer0wins() == 3)
            return ("Player 1 wins");
        else if(pGS.getPlayer1wins() == 3)
            return ("Player 2 wins");
        else
            return null;
    }

}
