package edu.johnsong22.pazaak;

import java.io.Serializable;

import edu.johnsong22.pazaak.GameFramework.GamePlayer;
import edu.johnsong22.pazaak.GameFramework.actionMessage.GameAction;

/*
    Authors: Glen Johson, Jim Rowe, Glen Stone, James Conn
    Action class for flipping flippable Side Deck cards
*/
public class FlipCardAction extends GameAction implements Serializable {

    private Card card;

    //constructor
    public FlipCardAction(GamePlayer player, Card toPlay){
        super(player);
        card = toPlay;
    }

    public Card getCard() {
        return card;
    }
}
