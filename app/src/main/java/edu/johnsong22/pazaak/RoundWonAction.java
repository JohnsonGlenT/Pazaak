package edu.johnsong22.pazaak;

import java.io.Serializable;

import edu.johnsong22.pazaak.GameFramework.GamePlayer;
import edu.johnsong22.pazaak.GameFramework.actionMessage.GameAction;

/*
    Authors: Glen Johson, Jim Rowe, Glen Stone, James Conn
    Action class that refreshes the game board when a round has been one
*/
public class RoundWonAction  extends GameAction implements Serializable {

    //constructor
    public RoundWonAction(GamePlayer player){
        super(player);
    }

}
