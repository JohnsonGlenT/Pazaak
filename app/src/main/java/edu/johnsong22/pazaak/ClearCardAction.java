package edu.johnsong22.pazaak;

import java.io.Serializable;

import edu.johnsong22.pazaak.GameFramework.GamePlayer;
import edu.johnsong22.pazaak.GameFramework.actionMessage.GameAction;

/*
   Authors: Glen Johnson, Jim Rowe, Grant Stone, James Conn
   Action class for removing selected Side Deck Cards
*/

public class ClearCardAction extends GameAction implements Serializable {

    Card card;
    
    //constructor
    public ClearCardAction(GamePlayer player, Card c){
        super(player);

        if (c != null) {
            card = new Card(c);
        } else {
            card = null;
        }
    }

    public Card getCard() {
        return card;
    }
}
