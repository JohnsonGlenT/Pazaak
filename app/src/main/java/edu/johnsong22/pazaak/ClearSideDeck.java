package edu.johnsong22.pazaak;

import java.io.Serializable;

import edu.johnsong22.pazaak.GameFramework.GamePlayer;
import edu.johnsong22.pazaak.GameFramework.actionMessage.GameAction;

/*
  Authors: James Conn, Grant Stone, Jim Rowe, Glen Johnson
  Action class to clear Side Deck Selection
*/
public class ClearSideDeck extends GameAction implements Serializable {
  
  //constructor
  public ClearSideDeck(GamePlayer player){
    super(player);
  }
  
}
