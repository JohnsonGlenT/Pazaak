package edu.johnsong22.pazaak;

import java.io.Serializable;

import edu.johnsong22.pazaak.GameFramework.GamePlayer;
import edu.johnsong22.pazaak.GameFramework.actionMessage.GameAction;

/*
  Authors: Glen Johson, Jim Rowe, Glen Stone, James Conn
  Action class for playing a card
*/
public class PlayCardAction extends GameAction implements Serializable {

  private Card card;
  
  //constructor
  public PlayCardAction(GamePlayer player, Card toPlay){
    super(player);
    card = toPlay;
  }
  
  //getter for the card
  public Card getCard() {
    return card;
  }
}
